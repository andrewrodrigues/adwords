import React, { Component } from 'react'
import ProductsContainer from './ProductsContainer'
import CartContainer from './CartContainer'

export default class App extends Component {
  render() {
    return (
        <section id="container">
            <header class="header fixed-top clearfix">
                <div class="brand">
                    <a href="index.html" class="logo">
                        AdWords Log
                    </a>
                    <div class="sidebar-toggle-box">
                        <div class="fa fa-bars"></div>
                    </div>
                </div>
            </header>
        </section>
    )
  }
}
